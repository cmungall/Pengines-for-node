Performing remote Prolog against a server can now be done also in Node.

## Example

```
peng = pengines(
        server: "http://localhost:3020/pengine",
        chunk: 2,
        src_text: 'animal(cat).\n' + 'animal(dog).\n'
        ask: 'animal(Animal)'
    )
    .on 'success', (result) ->
        for resultData in result.data
            console.log resultData.Animal
```

output:

```
cat
dog
```

## Functionality


### pengines(options)

Constructs a pengine. Options:

- `server` - A URL pointing to the Pengines server on which to create the pengine.
- `application` - The name of the application in which the pengine is to be run. Default is `swish`.
- `ask` - The query passed to the pengine immediately after its creation. By default no query is passed. Using this option will typically save one network roundtrip and thus using it with a deterministic query will result in just one roundtrip being made.
- `template`- A Prolog variable (or a term containing Prolog variables) shared with the query. Meaningful only if the ask option is specified. By default the value of this option is the variable bindings of the query passed in the ask option (a list of Name=Var pairs). Variable names in the query starting with an underscore character will however not appear in the list.
- `chunk`- The maximum number of solutions to retrieve in one chunk. 1 means no chunking (default).
- `destroy`- Determines if the pengine is to destroy itself after having run a query to completion. Defaults to `true`.
- `sourceText` - Prolog source code to be injected in the pengine before attempting to solve any queries.
- `format` - Determines the format of event responses. Can be one of the following options:
    * `json` (default)
    * `json-s`
    * `json-html`
    * New formats can be added by defining `event_to_json/3`. See `library(pengines_io)`.
- `cookieJar`- An instance of a jar containing cookies which will be used in the queries, usually retreived from the [request.jar()](https://github.com/request/request#requestjar).


### on(event,function)

Make `function` trigger on `event`. Supported values of `event` are:

- `create` - The pengine has been created.
- `success` - The pengine responds with a successful answer to a query. The callback is an object containing the following fields:
    * `data` - A list of objects each representing a solution to the query,
    * `more` - A boolean indicating whether more solutions may exist
    * `id `- The id of the pengine returning the answer.
- `error` - The pengine throws an error. The expression this.data in the scope of the function evaluates to an error message in the form of a string.
- `prompt` - The pengine evaluates the `pengine_input/2` predicate. The expression this.data in the scope of the function evaluates to a prompt in the form of a string or an object.
- `failure` - The pengine fails to find a solution.
- `stop` - A running query has been successfully stopped.
- `abort` - A running query has been successfully aborted.
- `destroy` - The pengine has been successfully destroyed.

### ask(expression)

Forwards a Prolog expression to the server.

### next()

Asks the pengine for the next solution from last asked expression.


### abort()

Terminates the running query by force.

### destroy()

Destroys the pengine.

### respond(item)

Inputs a term in response to a prompt from an invocation of `pengine_input/2` that is now waiting to receive data from the outside. Generates an error if string cannot be parsed as a Prolog term or if object cannot be serialised into JSON.

### stop()

Stops the query currently running without destroying the object.

## Set it off!

```npm install pengines```

## Credits

Modified version of [pengines.js](http://swish.swi-prolog.org/pengine/pengines.js), developed by Torbjörn Lager. Most options and events are identical to as documented in [this version](http://pengines.swi-prolog.org/docs/documentation.html). For more information on the application flow, see [this resource](http://www.swi-prolog.org/pldoc/man?section=pengine-overview).
