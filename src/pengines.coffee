###
    Copyright (c) 2014, Torbjörn Lager
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:

    1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
    FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
    COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
    LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
    ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    Modified by Karl Lundfall, 2015.
###




request = require 'request'
s = require 'string'
events = require 'events'



class Pengine extends events.EventEmitter
    #static entity
    @ids = []
    
    constructor : (options) ->
        
        options.format ?= "json"
        options.server ?= "/pengine"
        
        
        
        {@format, @server, @cookieJar} = options
        
        createOptions = {}
        
        
        
        
        
        #Copy certain fields of the constructor options to the creation options
        createOptions[key]=value for key,value of options when key in ['format','sourceText','application','ask','template','chunk'] or (key=='destroy' and typeof value == 'boolean')
        
        #This might not be necessary...
        createOptions.breakpoints = []
        
        #Rename the key sourceText to src_text
        createOptions.src_text = createOptions.sourceText
        
        delete createOptions.sourceText
        
        
        
        
        
        request {
                url: @server + '/create',
                json: true,
                body: createOptions,
                method: "POST",
                jar: @cookieJar
            },
            (error,...,response) =>
                if error
                    #return @onError? {data: "Failed to connect to #{@server}: #{error}"}
                    return @emit('error',{data: "Failed to connect to #{@server}: #{error}"})
                @_processResponse(response)
        
        
    #Removed function destroy_all, it is now the responsilibty of the user to kill remaining pengines
            
    #Static function for translating some paramters in @send. Done in one line!
    @optionsToList = (options) -> '[' + (key + '(' + value + ')' for key,value of options) + ']'
    
    #Internal function
    _processResponse : (obj) ->
        switch obj.event
            when 'create'
                @id = obj.id
                Pengine.ids.push @id
                if Pengine.ids.length > obj.slave_limit
                #if false
                    @destroy()
                    obj.data = "Attempt to create too many pengines. The limit is: #{obj.slave_limit}"
                    obj.code = "too_many_pengines";
                    #@onError? obj
                    @emit('error',obj)
                    console.error obj.data
                else
                    #@onCreate? obj
                    @emit('create',obj)
                    if obj.answer? then @_processResponse obj.answer
            when 'stop' then @emit('stop',obj)
            #when 'stop' then @onStop? obj
            when 'success' then @emit('success',obj)
            #when 'success' then @onSuccess? obj
            when 'failure' then @emit('failure',obj)
            #when 'failure' then @onFailure? obj
            when 'error'
                if obj.code == "existence_error"  and obj.arg1 == "pengine" and obj.arg2 == @id
                    @_unregister
                #@onError? obj
                @emit('error',obj)
                #@onError ? console.error obj.data
                console.error obj.data
            when 'output'
                @emit('output',obj)
                #@onOutput? obj
                @_pullResponse obj.id
            when 'debug'
                @emit('debug',obj)
                #@onDebug? obj
                console.log "[debug]: #{obj.data}"
                @_pullResponse obj.id
            #when 'prompt' then @onPrompt? obj
            when 'prompt' then @emit('prompt',obj)
            when 'abort'
                @aborted = true
                #@onAbort? obj
                @emit('abort',obj)
            when 'destroy'
                @_unregister()
                if obj.data then @_processResponse obj.data
                #@onDestroy? obj
                @emit('destroy',obj)
            when 'died'
                @_unregister
                if  not @aborted
                    obj.data = "Pengine has died"
                    obj.code = "died"
                    #@onError? obj
                    @emit('error',obj)
                    console.error obj.data
            else
                #Treat as error
                @_processResponse {event: 'error', data: obj}
                
    #Internal function
    _processPullResponse : (obj) ->
        if obj.event isnt 'died' then @_processResponse obj

    #Sends an event to <server>/send
    send : (event) ->
        request( {url: "#{@server}/send", method: "POST",json: true,form:{ id: @id, event: event, format: @format }, jar: @cookieJar}, (...,response) => @_processResponse(response))
        #Return this to allow for chaining
        return @
                 

    #Unregisters the id from the static Pengine.ids, internal function
    _unregister : ->
        index = Pengine.ids.indexOf @id
        if index > -1  then Pengine.ids.splice index, 1
        @died = true
    
    #Sends a query to the server
    ask: (query, options) ->
        @send "ask((#{query}), #{Pengine.optionsToList(options)})"
        
    #Gets more entries
    next: (n) =>
        @send 'next' + if n? then "(#{n})" else ''
    
    #Warning: I haven't really gotten this working, but this is supposed to stop a query
    stop : ->
        @send 'stop'
        
    
    #Reseponds by sending some input
    respond:  (input) ->
        @send "input((#{input}))"
    
    #Internal function
    _pullResponse:  (id) ->
        if not id? then id = @id
        console.log "#{@server}/pull_response?id=#{id}&format=#{@format}"
        request( {url: "#{@server}/pull_response", method: "POST",json: true,form:{ id: id, format: @format }, jar: @cookieJar}, (...,response) => @_processResponse(response))

    
    #Aborts the pengine
    abort: ->
        request {url: "#{@server}/abort?id=#{@id}&format=#{@format}", jar: @cookieJar}, @_processResponse
        #Return this to allow for chaining
        return @
        
    #Destroys the pengine
    destroy: ->
        if not @died
            return @send "destroy"
        #Return this to allow for chaining
        return @
    
    #Turns some data into string, according to the option
    @stringify : (data, options) ->
        msg  = "";
        strq =  if options? and options.string is "atom" then "'" else '"'

        serialize = (data) ->
            stringEscape = (s, q) ->
                dec2unicode = (i)  ->
                     switch
                         when 0<= i <= 15 then "\\u000#{i.toString(16)}"
                         when 16<= i <= 255 then "\\u00#{i.toString(16)}"
                         when 256<= i <= 4095 then "\\u0#{i.toString(16)}"
                         when 4096<= i <= 65535 then "\\u#{i.toString(16)}"

                result = q
                for c in s
                    if  c >= ' '
                        result += switch
                            when "\\" then "\\\\"
                            when q then ("\\"+q)
                            else c
                    else
                        result += switch
                            when "\n" then "\\n"
                            when "\r" then "\\r"
                            when "\t" then "\\t"
                            when "\b" then "\\b"
                            when "\f" then "\\f"
                            else dec2unicode c.charCodeAt 0
                return result+q

            serializeKey = (k) ->
                if k.match(/^\d+$/)
                    msg += k
                else
                    msg += stringEscape k, "'"
                    
                return true

            switch typeof data
                when "number"
                    msg += data
                when "string"
                    msg += stringEscape data, strq
                when "object"
                    if Array.isArray(data)
                        msg += "["
                        for i in [0...data.length]
                            if !serialize(data[i])
                                return false
                            if i+1 < data.length
                                msg += ","
                        msg += "]"
                    else
                        first = true
                        msg += "js{"
                        for p of data
                            if not first
                                msg += ","
                            else
                                first = false;
                            if not serializeKey(p)
                                return false
                            msg += ":"
                            if not serialize(data[p])
                                return false
                        msg += "}"
                else
                    return false
            return true

        if serialize data then return msg
    

#It is important to destroy all open pengines before exit!
module.exports = ((options) -> new Pengine(options))
